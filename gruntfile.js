module.exports = function (grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        sass: { 
            dist: {
                options: { 
                    style: 'extended' 
                },
                files: { 
                    'css/build/main.css': 'css/main.scss', // 'destination': 'source'                   
                }
            }
        },
        uglify:{
          options:{
              manage:false
          },
          all:{
              files:[{
                  expand: true,      
                  cwd:'scripts/',
                  src: ['*.js', '!*.min.js'],
                  dest: 'scripts/build/',
                  ext: '.min.js'
              }]
          }
        },
        autoprefixer: {
            dist: {
                files: {
                    'css/build/main.css': 'css/build/main.css'
                }
            }
        }
    });
     
    grunt.loadNpmTasks('grunt-contrib-sass');  
    grunt.loadNpmTasks('grunt-autoprefixer');
    grunt.loadNpmTasks('grunt-contrib-uglify');
 
    grunt.registerTask('default', ['sass', 'autoprefixer', 'uglify']);  
};
